rm( list = ls() )
setwd('/Users/know/Desktop/tor analytics/')
library(car)    # for pretty plots
#library(matlab) # for matlab function names
#library(data.table) # for data.table like data.frame
#library(xtable) # for exporting to LaTeX
#library(gdata)
#library(lmtest) # for testing linear models
#library(calibrate)

library(plyr) # for renaming columns

#######################################################

## Readin the data into data.frame D with columns: days, relays.stable, relays.fast, relays.all
#######################################################
Dstable <- read.csv('relays-stable.csv')[c('date','relays')]
#Dfast <- read.csv('relays-fast.csv')[c('date','relays')]
#Dtemp <- merge(Dstable, Dfast, by="date", suffixes=c('.stable','.fast') )
Dall <- read.csv('relays-total.csv')[c('date','relays')]
D <- merge( Dstable, Dall, by='date' )
D <- rename(D, c("date"="day","relays"="relays.all"))
names(D)

plot(D$day, log2(D$relays.all), ylab='Number of Relays', xlab='Year', yaxt='n', pch=20, cex=0.6, xaxt='n', col='blue', ylim=c(8,13) ) 

points( D$day, log2(D$relays.stable), pch=20, cex=0.6, col='purple' )


plot(1:nrow(Dall), log2(Dall$relays), ylab='Number of Relays', xlab='Year', yaxt='n', pch=20, cex=0.6, xaxt='n', col='blue', ylim=c(8,13) )


####### Set the pretty X-axis ###################################
YearLabels=seq(from=2008,to=2014,by=1)
YearLocations=c(66,432,797,1162,1527,1893,2258)
axis(1,at=YearLocations,labels=YearLabels )
#################################################################

####### Set the pretty Y-axis ###################################
par(las=1)
lab <- seq(from=1,to=45,by=1)
axis(2,at=lab,labels=parse(text=paste("2^", lab, sep="")) )
#################################################################






## Set the Legend and Title
##################################################################
legend_texts = c(
  expression(paste("All relays         ", r^2, "=0.96")),
  expression(paste("Stable relays   ", r^2, "=0.93"))  
)

legend( "topleft", legend=legend_texts, inset=0.05, pch=c(20,20), col=c('blue','purple') ) 

multiTitle(color="black","Number of Tor relays doubles every ", 
           color="purple","2.1",
           color="black",'-',
           color="blue","2.6",
           color="black"," years" )



####### Plot the best-fit lines ############################

# remove data before year 2010
FD <- D[ which(D$day >= 750), ]

# remove points with super-high-residuals from the fitted line
fit_all <- lm( log2(relays.all) ~ day, data=FD )
rows_to_remove <- abs(resid(fit_all)) > 0.38
FD <- FD[ !rows_to_remove, ]


# fit to a linear model
fit_all <- lm( log2(relays.all) ~ day, data=FD )
fit_stable <- lm( log2(relays.stable) ~ day, data=FD )


# Add the best-fit lines
first_day <- min(FD$day)
segments( first_day, predict(fit_all, data.frame(day=first_day)),
          max(FD$day), predict(fit_all, data.frame(day=max(FD$day))),
          col="black", lty=2, lwd=3 )
segments( first_day, predict(fit_stable, data.frame(day=first_day)),
          max(FD$day), predict(fit_stable, data.frame(day=max(FD$day))),
          col="black", lty=2, lwd=3 )

# Add the black squares
points( first_day, predict(fit_all, data.frame(day=min(FD$day))), col="black", pch=15, cex=1.3)
points( first_day, predict(fit_stable, data.frame(day=min(FD$day))), col="black", pch=15, cex=1.3)

#summary( fit_all )
#summary( fit_stable )