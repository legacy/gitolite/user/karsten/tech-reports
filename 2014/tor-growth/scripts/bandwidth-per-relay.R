rm( list = ls() )
setwd('/Users/know/Desktop/tor analytics/')
library(car)    # for pretty plots
library(matlab) # for matlab function names
library(data.table) # for data.table like data.frame
#library(xtable) # for exporting to LaTeX
#library(gdata)
library(lmtest) # for testing linear models
library(calibrate)

library(plyr) # for renaming columns
source("colortitles.R")
#######################################################

## Readin the data into data.frame D with columns: days, relays.stable, relays.fast, relays.all
#######################################################
#Dstable <- read.csv('relays-stable.csv')[c('date','relays')]
#Dfast <- read.csv('relays-fast.csv')[c('date','relays')]
#Dtemp <- merge(Dstable, Dfast, by="date", suffixes=c('.stable','.fast') )

Dbandwidth <- read.csv('bandwidth-clean.csv')[c('date','advbw','bwread')]
Drelays <- read.csv('relays-total.csv')[c('date','relays')]
D <- merge( Dbandwidth, Drelays, by='date' )
D <- rename(D, c("date"="day","relays"="relays.all"))
names(D)

# convert units from from B/s to MiB/s
D$advbw <- D$advbw / 1048576.0
D$bwread <- D$bwread / 1048576.0


D[c("advbw_per_relay","bwread_per_relay")] <- c(NA,NA)
D$advbw_per_relay <- D$advbw / D$relays.all
D$bwread_per_relay <- D$bwread / D$relays.all

plot(D$day, log2(D$advbw_per_relay), ylab='Bandwidth (MiB/s) per relay', xlab='Year', yaxt='n', pch=20, cex=0.6, xaxt='n', col='blue' )
points( D$day, log2(D$bwread_per_relay), pch=20, cex=0.6, col='red' )


####### Set the pretty X-axis ###################################
YearLabels=seq(from=2008,to=2014,by=1)
YearLocations=c(66,432,797,1162,1527,1893,2258)
axis(1,at=YearLocations,labels=YearLabels )
#################################################################

####### Set the pretty Y-axis ###################################
par(las=1)
lab <- seq(from=-3,to=1,by=1)
axis(2,at=lab, labels=c("⅛","¼","½","1","2") )
#################################################################

## Set the Legend and Title
##################################################################
legend_texts = c(
  expression(paste("Capacity (advertised) ", r^2, "=0.91")),
  expression(paste("Used (read)                ", r^2, "=0.68"))  
)


legend( "topleft", legend=legend_texts, inset=0.05, pch=c(20,20), col=c('blue','red') ) 


multiTitle(color="black","Average bandwidth per relay doubles every ", 
           color="blue","1.8",
           color="black",'-',
           color="red","2.1",
           color="black"," years")

####### Plot the best-fit lines ############################

# remove data before the 'read' metric started
temp <- subset( D, !is.na(bwread) )
first_day <- min(temp$day)
FD <- D[ which(D$day >= first_day), ]

# fit the Filtered Data to a linear model...
fit_advbw <- lm( log2(advbw_per_relay) ~ day, data=FD )
fit_bwread <- lm( log2(bwread_per_relay) ~ day, data=FD )


# Add the best-fit lines
segments( first_day, predict(fit_advbw, data.frame(day=first_day)),
          max(FD$day), predict(fit_advbw, data.frame(day=max(FD$day))),
          col="black", lty=2, lwd=3 )
segments( first_day, predict(fit_bwread, data.frame(day=first_day)),
          max(FD$day), predict(fit_bwread, data.frame(day=max(FD$day))),
          col="black", lty=2, lwd=3 )

# Add the black squares
points( first_day, predict(fit_advbw, data.frame(day=min(FD$day))), col="black", pch=15, cex=1.3)
points( first_day, predict(fit_bwread, data.frame(day=min(FD$day))), col="black", pch=15, cex=1.3)

#summary( fit_all )
#summary( fit_stable )

