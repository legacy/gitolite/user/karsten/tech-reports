rm( list = ls() )
setwd('/Users/know/Desktop/tor analytics/')


process_torperf_rawdata <- function( filename, filesize_to_consider=5242880 )
{
  
  # Import data from TorPerf
  Dtp <- read.csv( filename )[c('day','date','size','source','q1','md','q3')]
  Dtp <- rename(Dtp, c("size"="filesize") )
  
  print(unique(Dtp$filesize))
  # only use the aggregated Tor data for downloading a 5 MiB file
  Dtp <- subset( Dtp, source=='' & filesize==filesize_to_consider )
  
  #print( tail(Dtp) )
  
  # drop the source and filesize column
  #Dtp <- Dtp[ , -which(names(Dtp) %in% c('source','filesize'))]
  Dtp <- Dtp[ , -which(names(Dtp) %in% c('source'))]
  
  # rename the q1, md, and q3 for TIME
  Dtp <- rename(Dtp, c("q1"="time_q1","md"="time_md","q3"="time_q3") )
  
  # convert time from MILLISECONDS -> SECONDS
  Dtp$time_q1 <- Dtp$time_q1/1000
  Dtp$time_md <- Dtp$time_md/1000
  Dtp$time_q3 <- Dtp$time_q3/1000
  
  
  # now create the bw_q1, bw_md, bw_q3 in: KiB/s
  Dtp[c("bw_q1","bw_md","bw_q3")] <- c(NA,NA,NA)
  
  #convert my_filesize to 
  
  # Rewrite q1, md, and q3 to be in bandwidth (KiB/s)
  Dtp$bw_q1 <- (filesize_to_consider / 1024) / Dtp$time_q1;
  Dtp$bw_md <- (filesize_to_consider / 1024) / Dtp$time_md;
  Dtp$bw_q3 <- (filesize_to_consider / 1024) / Dtp$time_q3;
  
  return(Dtp)
}

#####################################################################################
# Number of relays
#####################################################################################

# remove all entries with values below 937
Dall <- read.csv('relays-total.csv')[c('date','relays')]
Dall$day <- NA
Dall$day <- 1:nrow(Dall)

plot( Dall$day, log2(Dall$relays), pch=20, cex=0.6, col='blue' )

Dall <- subset(Dall, relays >= 938 )
points(Dall$day, log2(Dall$relays), pch=20, cex=0.6, col='red' )

mm <- lm( log2(Dall$relays) ~ Dall$day  )

rows_to_remove <- abs(resid(mm)) > 0.38
Dall <- Dall[ !rows_to_remove, ]

points(Dall$day, log2(Dall$relays), pch=20, cex=0.6, col='purple' )

mm <- lm( log2(Dall$relays) ~ Dall$day  )

# from here the doubling rate is gotten by
(1.0 / mm$coefficients) / 365

# which comes out to 2.99 years

#####################################################################################
# total network bandwidth
#####################################################################################
rm( list = ls() )
D <- read.csv('bandwidth-clean.csv')[c('date','advbw','bwread')]
D$advbw <- D$advbw / 1048576.0
#D <- rename(D, c("date"="day") )
D$day <- NA
D$day <- 1:nrow(D)

# convert from B/s to MiB/s

m <- lm( log2(D$advbw) ~ D$day )

# now for the monthly doubling rate
(1.0 / m$coefficients) / 30


#####################################################################################
# Absolute Torperf
#####################################################################################
#rm( list = ls() )
D_SMALL <- process_torperf_rawdata('torperf-clean.csv', 51200)[c('date','day','bw_md')]
D_BIG <- process_torperf_rawdata('torperf-clean.csv', 5242880)[c('date','day','bw_md')]


D_SMALL <- subset( D_SMALL, day>=547 )
D_BIG <- subset( D_BIG, day>=547 )


mSMALL <- lm( log2(bw_md) ~ day, data=D_SMALL )
mBIG <- lm( log2(bw_md) ~ day, data=D_BIG )

(1.0 / mSMALL$coefficients) / 30
(1.0 / mBIG$coefficients) / 30



#####################################################################################
# Normalized Torperf
#####################################################################################

# Readin the netindex and average it
#######################################################
D_netindex <- read.csv('country_daily_speeds.csv')[c('date','country_code','download_kbps')]

D_netindex$download_kbps <- D_netindex$download_kbps * (1000 / 1024)


# make a single download_rate averaging across the countries: US, DE, RU.
D_US = subset( D_netindex, country_code=='US' )
D_DE = subset( D_netindex, country_code=='DE' )
D_RU = subset( D_netindex, country_code=='RU' )

# merge the US, DE, and RU bandwidths
D_temp <- merge( D_US, D_DE, by='date' )
D_ni <- merge( D_temp, D_RU, by='date' )

# drop the country codes
D_ni <- D_ni[ , -which(names(D_ni) %in% c('country_code.x','country_code.y','country_code'))]

# average the download KiB/s entries into one
D_ni$avr_download_KBps <- NA
D_ni$avr_download_KBps <- (D_ni$download_kbps.x + D_ni$download_kbps.y + D_ni$download_kbps) / 3.0

# drop the country-specific download rates
D_ni <- D_ni[ , -which(names(D_ni) %in% c('download_kbps.x','download_kbps.y','download_kbps'))]


# now merge D_ni and {D_SMALL,D_BIG} based on date
Dnorm_SMALL <- merge( D_ni, D_SMALL, by='date' )
Dnorm_BIG <- merge( D_ni, D_BIG, by='date' )

Dnorm_SMALL$normalized_bw <- NA; Dnorm_SMALL$normalized_bw <- Dnorm_SMALL$bw_md / Dnorm_SMALL$avr_download_KBps
Dnorm_BIG$normalized_bw <- NA; Dnorm_BIG$normalized_bw <- Dnorm_BIG$bw_md / Dnorm_BIG$avr_download_KBps


