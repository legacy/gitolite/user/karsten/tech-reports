#!/usr/bin/python

### Use a simple hillclimbing algorithm to find the selection
### probability vector for Tor nodes which minimizes the expected
### latency. Takes as input a file, one node per line, specifying
### bandwidth capacity and usage, both in bytes/sec, space separated.

### Copyright (c) 2008-2009, Steven J. Murdoch
### 
### Redistribution and use in source and binary forms, with or without
### modification, are permitted provided that the following conditions are
### met:
### 
###     * Redistributions of source code must retain the above copyright
### notice, this list of conditions and the following disclaimer.
### 
###     * Redistributions in binary form must reproduce the above
### copyright notice, this list of conditions and the following disclaimer
### in the documentation and/or other materials provided with the
### distribution.
### 
###     * Neither the names of the copyright owners nor the names of its
### contributors may be used to endorse or promote products derived from
### this software without specific prior written permission.
### 
### THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
### "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
### LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
### A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
### OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
### SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
### LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
### DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
### THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
### (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
### OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys

if __name__=="__main__":
    import matplotlib
    matplotlib.use('TkAgg') # do this before importing pylab

import matplotlib.pyplot as plt
from numpy import *

import time

import threading

## Number of iterations before narrowing adjustment
ILIMIT = 1000
## Maximum number of iterations before exiting
NUM=8000
## Fraction of network capacity which is being used (set to None to
## use actual network data
USAGE=0.5

def wait(x, p, L, isbroken):
    '''Calculate waiting time at each node (assuming M/D/1 queue)'''
    z = p*L*x
    a = z*x
    b = 2.0*(1.0-z)
    return x + a/b

def calc_waittime(prob, xs, totalusage, debug=False):
    '''Function to optimize'''
    #print "info", prob[0], nodebw[0], totalusage
    ## Check that probabilities really add up to 1
    assert abs(1.0 - prob.sum()) < 1e-6

    ## Find overloaded nodes
    loading_factor = xs*prob*totalusage
    broken = (loading_factor < 0.0) | (loading_factor > 1.0)
    if broken.any():
        return None
    ## Number of broken nodes
    #print "Broken", len([x for x in broken if x])

    ## Calculate weighted waiting time
    wtime = wait(xs, prob, totalusage, False)
    wtime[broken] = -1.0

    ## Get maximimum waiting time for non-broken nodes
    cap = wtime.max()

    ## Calculate capped weighted waiting time
    wtime[broken] = cap
    wwtime = wtime * prob
    
    return wwtime

def load_data(fn):
    fh = file(fn, "rt")

    ## Load in node bandwidths and total network usage
    totalusage = float64(0.0)
    totalbw = 0.0
    nodebw = []
    for line in fh:
        bw, usage = line.split()
        bw = float(bw)/512.0
        usage = float(usage)/512.0
        ## Ignore nodes with BW outside acceptable range
        if bw<=0.0 or bw >= (10.0e6/512.0):
            continue
        totalusage += usage
        totalbw += bw
        nodebw.append(bw)

    # Node selection probabilities
    pu = array([1.0/len(nodebw)] * len(nodebw)) # uniform
    pt = array([bw / totalbw for bw in nodebw]) # Tor

    anodebw = array(nodebw)
    xs = 1.0/anodebw

    ## Weighted waiting time for Tor and uniform weighting
    x = calc_waittime(pt, xs, totalusage, True)
    y = calc_waittime(pu, xs, totalusage, True)

    print "E(Tor)", sum(x)
    print "E(Uniform)", sum(y)

    return totalusage, anodebw, x,y

def optimize(sprob, ss, xs, totalusage, amount):
    l = len(sprob)
    i = 0
    while True:
        i += 1
        ## Take copy of base
        prob = sprob.copy()
        
        ## Find elements to change
        a = random.randint(0, l)
        b = random.randint(0, l)

        ## Clip amount to modify by
        if prob[a] < amount:
            damount = prob[a]
        else:
            damount = amount

        ## Move between two elements
        prob[a] -= damount
        prob[b] += damount

        ## Calculate the function to optimize
        wwait = calc_waittime(prob, xs, totalusage, True)
        if wwait is None:
            ## We reached a bad state, give up
            continue

        ## If this is an improvement, return new probabilities
        s = wwait.sum()
        if s < ss:
            return i, prob, wwait, s
        if i > ILIMIT:
            ## We reached the maximum limit, return the original parameters
            wwait = calc_waittime(sprob, xs, totalusage, True)
            return i, sprob, wwait, wwait.sum()

def save(anodebw, prob, xs, totalusage, count):
    fh = file("nodeprob-%06d"%count, "wt")
    anodebw.dump(fh)
    prob.dump(fh)
    xs.dump(fh)
    totalusage.dump(fh)
    fh.close()
    #fh.write("bw prob\n")
    #for i in range(len(anodebw)):
    #    fh.write("%f %f\n"%(anodebw[i],prob[i]))
    #fh.close()

class Animator(threading.Thread):
    def __init__(self, prob, totalusage, anodebw, fig, ax):
        threading.Thread.__init__(self)
        self.prob = prob
        self.totalusage = totalusage
        self.anodebw = anodebw
        self.xs = 1.0/anodebw

        self.anodebw = anodebw
        self.fig = fig
        self.ax = ax
    
    def run(self):
        wwait = calc_waittime(self.prob, self.xs, self.totalusage)
        s = wwait.sum()
        #line, = self.ax.plot(self.anodebw, wwait.cumsum())
        line, = self.ax.plot(wwait.cumsum())
        amount = 1.0/2

        ## Main optimization loop
        i = 0
        while i < NUM:
            i += 1
            cnt, self.prob, wwait, s = optimize(self.prob, s, self.xs, self.totalusage, amount)
            print "%6d %4d %f"%(i, cnt, s)
            if cnt > ILIMIT:
                ## We tried for too long to optimize so reduce the
                ## amount to modify probabilities
                amount /= 2.0
                print "Narrowing... to", amount
                save(self.anodebw, self.prob, self.xs, self.totalusage, i)
                
            line.set_ydata(cumsum(wwait))
            self.fig.canvas.draw()
        ## Save the intermediate result
        save(self.anodebw, self.prob, self.xs, self.totalusage, i)
        sys.exit()

def main():
    if len(sys.argv) != 2:
        print "Usage: hillclimbing.py FILE"
        sys.exit()

    ## Load data file, where each line is:
    ##  BANDWIDTH USAGE
    ## Where both are in kB/s
    fn = sys.argv[1]
    totalusage, anodebw, x, y = load_data(fn)

    ## Try for different levels of bandwidth usage
    #totalusage = anodebw.sum()*(1-1e-6)
    #totalusage = anodebw.sum()*(1-1e-3)
    #totalusage = anodebw.sum()*(1-1e-1)
    #totalusage = anodebw.sum()*(0.75)
    #totalusage = anodebw.sum()*(0.5)
    #totalusage = anodebw.sum()*(0.25)
    if USAGE != None:
        totalusage = anodebw.sum()*USAGE
    
    pt = anodebw/anodebw.sum()
    
    plt.ioff()
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ## Optimize selection probabilities
    anim = Animator(pt, totalusage, anodebw, fig, ax)

    win = fig.canvas.manager.window
    fig.canvas.manager.window.after(100, anim.start)

    plt.show()

if __name__=="__main__":
    main()

# vim: ai ts=4 sts=4 et sw=4
