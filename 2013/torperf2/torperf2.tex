\documentclass{tortechrep}
\usepackage{url}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{hyperref}
\usepackage{longtable}

\begin{document}

\title{Requirements and Software Design for a\\
Better Tor Performance Measurement Tool}

\author{Karsten Loesing, Sathyanarayanan Gunasekaran, and Kevin Butler}

\contact{\href{mailto:karsten@torproject.org}{karsten@torproject.org},%
\href{mailto:gsathya@torproject.org}{gsathya@torproject.org},%
\href{mailto:kbutler03@qub.ac.uk}{kbutler03@qub.ac.uk}}

\reportid{2013-10-002}
\date{October 30, 2013}

\maketitle

\section{Introduction}

Four years ago, we presented a simple tool to measure performance of the
Tor network \cite{tor-2009-09-001}.
This tool, called Torperf, requests static files of three different sizes
over the Tor network and logs timestamps of various request substeps.
These data turned out to be quite useful to observe user-perceived network
performance over time.%
\footnote{\url{https://metrics.torproject.org/performance.html}}
However, static file downloads are not the typical use case of a user
browsing the web using Tor, so absolute numbers are not very meaningful.
Also, Torperf consists of a bunch of shell scripts which makes it neither
very user-friendly to set up and run, nor extensible to cover new use
cases.

For reference, we made an earlier approach 1.5 years later that suggested
redesigning the Python parts in Torperf, but that redesign never
happened.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/2565}}

In this report we outline requirements and a software design for a rewrite
of Torperf.
We loosely start with non-functional requirements, so aspects like
user-friendliness or extensibility, because these requirements drive the
rewrite more than the immediate need for new features.
After that, we discuss actual functional requirements, so experiments that
we want to perform to measure things in the Tor network on a regular and
automated basis.
Finally we suggest a software design that fulfills all these requirements.

\section{Requirements}

Before we talk about actual experiments
the new Torperf is supposed to perform, we want to discuss more general
requirements of a Torperf rewrite.
For the time being, assume that the major functional requirement is to
``make client requests over the Tor network and record timestamps and
other meta data for later analysis.''
Whichever type of request this is, it's important that all or most of the
non-functional requirements listed in the following are met.
We start with configuration requirements, followed by requirements to
results formats.

\subsection{Configuration requirements}

\subsubsection{Installation and upgrade}

Whoever installs Torperf shouldn't be required to understand its codebase,
nor rely on support by someone who does.\footnote{This sounds obvious, but
this was the case with the current, shell script based Torperf, and it's
also the reason why its current known userbase is 2.}
Ideally, the new Torperf comes as OS package, meaning that it only relies
on third-party software that is also either available as OS package or
that is shipped with the Torperf package.
To put it simply, installing on Debian Wheezy should be as easy as
\verb+apt-get install+, and upgrading should simply be an
\verb+apt-get update && apt-get upgrade+ away.

\subsubsection{Run as service}

The new Torperf should run as OS service, unrelated to a specific user.
It should have a config file that is easy to understand, and
it should come with scripts to start, restart, and stop the Torperf
service.
If the service operator wants to run multiple experiments in parallel,
they'd
simply configure all those experiments in the configuration file or
directory, rather than starting multiple instances of Torperf.

\subsubsection{Single configuration point}

All requests that Torperf performs use a local Tor client, go over the Tor
network, and are answered by some server.
Some experiments may be designed to use remote servers not controlled by
the person running Torperf, but others require setting up a custom server.
In the latter case, configuring, starting, restarting, or stopping client
and server should happen in a single place, that is, on a single machine
as part of the Torperf service.
Fortunately, running client and server on the same physical should not
have any effect on measurement results, because all requests and responses
traverse the Tor network.

Reasons for \emph{not} separating client and server are:
\begin{itemize}
\item it's easier to deploy client and server together than deploying them
separately and configuring the client to use the server;
\item measurement results can easily contain client and server timestamps
of a measurement;
\item assuming the host has sufficient free bandwidth capacity, measurement
results shouldn't be affected by client and server running on the same
host; and
\item it's not unusual to deploy client and server talking over Tor in a
single tool: think of how a tor relay does bandwidth self-tests to itself.
\end{itemize}

What's not easily possible with this approach is to configure client and
server to run on different hosts.
It's unclear yet whether there is an experiment where this might be
useful.

% Commented out Kevin's text on 2013-09-24:
% \subsubsection{Experiment extensibility considerations}
%
% It should be easy for a user to implement or install an experiment that isn't
% bundled with the core distribution. Ideally, installing an experiment should be as
% simple as unzipping a folder or config file into an experiments folder.
% Optimally, the Torperf service would detect the new experiment and schedule
% it as soon as practical without additional user intervention.
% Sathya on tor-dev@:
% "I don't understand how this will work when users just apt-get install
% torperf. Ideally if someone writes a good experiment, they should send
% the patches upstream and get it merged, and then we update torperf to
% include those tests and then the users just update torperf with their
% package managers."
% Karsten on tor-dev@:
% "I don't feel strongly.  I'd prefer a design that makes it easy to add
% new experiments, but I'm fine with an approach that requires merging
% patches.  We can always add the functionality to drop something in a
% directory and make Torperf magically detect and run the new experiment,
% but that can happen later.  Maybe we shouldn't let that distract us from
% getting the first version done.  I commented out this section."

% Commented out Kevin's text by Karsten on 2013-09-18:
% For implementing, this will require a clearly defined interface for developers
% to work with.
% \footnote{Ideally, this interface would allow experiment implementations in
% any language, not only the language of choice for the core Torperf service.}
% This could be as simple as conforming to a common data format or a tight
% integration with the core service components which could allow lighter weight
% experiment implementations. If the experiment reuses the service's
% file server implementation, the service should post-process the result
% file(s) to add any additional server-side timing information available.
%
% Reason: We're not trying to build a generic experiment runner, but a
% specific Tor performance measurement tool.  Most experiments will depend
% on various Torperf components to start a tor process and parse tor
% events to storing results in the database.  There's no (reasonable) way
% for those experiments to interface with Torperf components if they don't
% speak Torperf's programming language.  If people want to add a simple
% experiment, they can write a wrapper for using their tool in Torperf.

% This requirement is still subject to discussion, because it's unclear how
% this will work when users just \verb+apt-get install torperf+.
% Ideally if someone writes a good experiment, they should send the patches
% upstream and get it merged, and then we update Torperf to include those
% tests and then the users just update Torperf with their package managers.

\subsubsection{User-defined tor version or binary}

A key part of measurements is the tor software version or binary used to
make requests or even to handle requests in the hidden-service case.
It should be easy to specify a tor version that is not the current tor
version shipped with the OS.
Similarly, it should be easy to point to a custom tor binary to use for
measurements.

It should be possible to run different experiments with different tor
versions or binaries in the same Torperf service instance.%
\footnote{Loosely specified tor versions, e.g. 'latest-stable, git:master,
$\geq$ 0.2.4' (following a well-defined schema),
would be a boon in reducing error prone configuration updates everytime
there are new tor versions released.}
An experiment should be able to be configured to run for multiple
versions of tor, potentially simultaneously, to spot performance change
across tor releases. Note that the tor version should be contained in the
results.

It might be beneficial to provide a mechanism to download and verify the
signature of new tor versions as they are released. The user could specify
if they plan to test stable, beta, or alpha versions of tor with their
Torperf instance.

This requirement should be implemented in three phases: 1) Torperf uses
the default tor binary that comes with the operating system; 2) Torperf
accepts the path to a tor binary that was previously built by the user;
3) Torperf accepts the path to a Git tag or tor source directory and
downloads, verifies, and builds a tor binary itself.

\subsubsection{User-defined third-party software version or binary}

Similar to the previous requirement, it should be easy to specify custom
versions or binaries of third-party software other than the version
currently shipped with the OS.
This applies, for example, to Firefox when attempting to make measurements
more realistic.

\subsection{Results requirements}

\subsubsection{Results format}

The measurement results produced by Torperf contain no sensitive data by
design, because all requests are made by Torperf itself, not by actual Tor
users.
We'll want to collect as much information as needed to perform useful
analysis.
But at the same time we want to keep it as easy as possible to process
results.

Results may come from various sources, e.g., an HTTP/SOCKS client,
Selenium%
\footnote{\url{http://www.seleniumhq.org/}}%
/Firefox, a tor client used to make the request or to answer the
request as hidden service, an HTTP server, etc.
Torperf should not store original logs, because that would only
shift the issue of processing different log formats to the analysis step.
Such an approach would also generate unnecessarily large results files.
Torperf should rather process data from these sources and store them in a
custom results format that can be easily processed using tools shipped
with Torperf.

Results may include data which appear not immediately relevant to
measuring Tor performance, but which may be useful for related purposes.
For example, Torperf should include data about circuit failures in its
results, even though these circuits may not have been used in actual
requests.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/8662}}

Deciding which data to store should be the responsibility of whoever
designs a Torperf experiment, though formats should be somewhat uniform
between experiments.
Torperf should store its results in a format that is widely used and
already has libraries (like JSON), so that other applications can use
the results and build on it.%
\footnote{In particular, we could use a de-facto standard like the HAR
(HTTP Archive) format to store measurements results.
Advantages are that such a format is widely used and that there are a lot
of off-the-shelve tools that can process this format.
Disadvantages are that such a format captures a lot of meta data that are
not relevant to us, and that we'd have to add tor-specific data fields to
the format which wouldn't be processed by existing tools.
Note that we could use such a format only for request data, not for
tor-specific data like building circuits and attaching streams.}

\subsubsection{Results web interface}

Torperf should provide all its results via a public web interface.%
\footnote{It is a requirement to also provide a machine readable representation
to enable easier development of custom visualisations of the result data.}
This includes measured timestamps as well as any meta data collected in
an experiment.
Results should be available for all measurements as well as for a subset
specified by measurement time or filtered by other criteria. The interface
should highlight experiment failures.
% Commented out Kevin's text by Karsten on 2013-09-18:
% and provide a convenient mechanism to
% run a set of experiments on demand.
% Reason: The web interface has to be read-only for security reasons.

\subsubsection{Results accumulator}

A Torperf service instance should be able to accumulate results from its
own experiments and remote Torperf service instances.
Therefore, it should simply download new results from their web interfaces
and incorporate them in its own database.
It would be useful to have the accumulator warn if a remote service
instance becomes stale and doesn't serve recent results anymore.
Note how the service instance name or identifier should become part of
Torperf results meta data.
The accumulating Torperf instance must verify that all requested data
contains the expected Torperf service identifier and otherwise discard the
received data.

\subsubsection{Truncate old results}

A Torperf service instance should be able to limit space requirements for
storing past results.
This could be done by discarding results that are older than a configured
number of days.

\subsubsection{Results graph data}

Ideally, Torperf provides aggregate statistics via its web interface which
can then be visualized by others.
Even more ideally, Torperf serves a few HTML pages containing the
necessary JavaScript to visualize results.
While this may sound like going overboard here, making Torperf the tool
to run performance measurements \emph{and} to present results has the
advantage of not building and maintaining a separate tool for the latter.%
\footnote{For reference, the current Torperf produces measurement results
which are re-formatted by metrics-db and visualized by metrics-web with
help of metrics-lib.
Any change to Torperf triggers subsequent changes to the other three
codebases, which is suboptimal.}

\subsubsection{Results parsing library}

The new Torperf should come with an easy-to-use library to process its
results. The library should also make it simple to create Torperf compatible
results or clearly highlight areas of non conformance with existing results.
Alternatively, this library could be provided and maintained as part of
stem%
\footnote{\url{https://stem.torproject.org/}}
or another parsing library.

\section{Experiments}

\subsection{High priority experiments}

\subsubsection{Alexa top-X websites using Selenium and the Tor Browser}

One major reason for rewriting Torperf is to make its results more
realistic.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/7168}}
It was suggested to track down Will Scott's torperf-like scripts, make
them public if needed, and do a trial deployment somewhere.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/7516}}
An experiment making these more realistic measurements should use
something like Selenium to control an actual Tor Browser to make
requests.%
\footnote{Related to this, Zack Weinberg has written code that downloads
the present Tor Browser alpha and Python Selenium client and
monkey-patches them to play nice together; see
\url{https://github.com/zackw/tailwagger}.
This code is very specialized for his requirements right now, but might
still be of use to us.
We should look especially at what is done to TBB and python-selenium by
the patches under pkg/tor-browser-3.0a3 and pkg/python-selenium-2.33.0,
and the TbbDriver class in client/tbbselenium.py.}
As a variant, this experiment should be run with other Firefox versions, e.g.
that one that uses optimistic data.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/3875}}

\subsubsection{Static file downloads}
\label{sec:static-file-downloads}

Another major reason for rewriting Torperf is to supersede the existing,
bit-rotting codebase.
The new Torperf should therefore provide an experiment that is identical
to the current, single Torperf experiment: download static files of sizes
50 KiB, 1 MiB, and 5 MiB over the Tor network and record timestamps and
relevant meta data.

There are quite a few details in getting these downloads right, which
shall not all be specified here.
For example, Torperf needs to enforce using a fresh circuit for each run,
which is currently ensured by reducing the maximum circuit dirtiness to a
value that is lower than the experiment period.
An alternative may be to send the \verb+NEWNYM+ signal to the tor process
after every stream.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/2766}}
The easiest path might be to leverage on the stream isolation features
introduced in tor 0.2.3%
\footnote{\url{https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/171-separate-streams.txt\#l93}}
and use a different SOCKS username/password for each requests.

The best way to extract these requirements is to read the source.%
\footnote{\url{https://gitweb.torproject.org/torperf.git/tree}}
There also exists a proof-of-concept implementation of this experiment in
Twisted which can and should serve as starting point for this rewrite.%
\footnote{\url{https://gitweb.torproject.org/karsten/torperf.git/tree/refs/heads/perfd}}

The results format of the current Torperf codebase shall serve as
blueprint for designing a new results format.%
\footnote{\url{https://metrics.torproject.org/formats.html\#torperf}}
As stated before, the new results format should make use of JSON or
another data format, so that results can be processed more easily.

Results may even contain more information than the original Torperf
results.
For example, assuming that the new Torperf service controls both HTTP
client and server, new timestamps could be added for serving the first
and last byte.
Matching timestamps between client and server can be achieved by serving
new random content in each request and use this content (or a digest
thereof) as request identifier.

\subsection{Lower priority experiments}

\subsubsection{Canonical median web page}

A lower-priority experiment would be to devise and deploy the canonical
median web page.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/7517}}
Such an experiment would share a lot with static file downloads, but
would serve an actual web page using Torperf's own webserver.

\subsubsection{Hidden service performance}

Another possible experiment would be to measure performance to a
hidden service.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/1944}}
This hidden service could easily run on the same tor client instance,
or on a separate tor client instance running on the same physical host.
We'll want to add additional timestamps for hidden service events.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/2554}}
The hidden service could serve static files, a canonical median web page,
or even an Alexa top-X website.

The latter has the minor disadvantage that server timestamps cannot be
matched with client timestamps based on content, but only
probabilistically via timing.
This may become problematic when running lots and lots of experiments at
the same time.
Or maybe it's possible to match client and server observations using
identifiers, like the rendezvous cookie, exchanged in the rendezvous
process instead.

\subsubsection{GET POST performance}

The experiments so far all measured download speed, but it's also easy
to measure upload speed.%
\footnote{\url{https://trac.torproject.org/projects/tor/ticket/7010}}
Torperf's own web server could accept POST data and measure timestamp of
first byte received, last byte received, etc.
Similar to static file downloads, client and server timestamps can be
matched based on the random data that is posted to the server, or a digest
of that data.

Ideally such upload tests should also be done using a full web browser
driven by Selenium.

\section{Software design}

The previous sections outlined requirements to the Torperf rewrite and
possible experiments, unrelated to an actual implementation.
The purpose of this section is to suggest a possible software design
matching these requirements.
It's quite possible that there are better software designs.
This section should serve as starting point for a discussion.

We have an initial proof-of-concept prototype that uses Twisted to
implement a small subset of the new Torperf.
Even though the new Torperf does quite a few things at once, like starting
tor processes, making requests, answering requests, collecting results,
etc., it can be implemented as a single Twisted application.
This shifts some deployment problems to what people usually do when
deploying Twisted applications, rather than forcing us to solve these
problems yet another time.
Of course, at the same time it forces us to follow the Twisted model of
writing applications, which seems not too bad in this case.
The current prototype also requires twisted-socks%
\footnote{\url{https://github.com/ln5/twisted-socks}}
as SOCKS client,
txtorcon%
\footnote{\url{https://github.com/meejah/txtorcon}}
for communicating with tor clients, and stem for event parsing.

The following list outlines tasks that the new Torperf needs to perform.
These could be implemented as Python classes, though this list was not
written with Python or Twisted specifics in mind:%
\footnote{An alternative to Python/Twisted might be Go, though it doesn't
have tor controller libs like txtorcon or stem.}

\begin{description}
\item[configuration handler] Validate and parse configuration file or
directory, provide configuration values to other application parts.
\item[logger] Log operation details for debugging purposes and for normal
service operation, unrelated to storing measurement results.
\item[tor process starter] Configure, start, hup, and stop local tor
client processes, using a previously configured tor version or binary.
\item[tor controller] Connect to tor's control port, force-set guards if
required by the experiment, register for and handle asynchronous events,
set up and tear down hidden services.
\item[experiment scheduler] Run experiments following a schedule.
% Commented out Kevin's text by Karsten on 2013-09-18:
% Must also
% handle immediate scheduling requests by users.
% Reason: Following a pre-defined schedule should be sufficient.  See also
% above why users shouldn't trigger new measurements from the web
% interface.
\item[experiment runner] Handle the execution of a scheduled experiment. It
should take care of the entire experiments lifecycle and reserve an
exclusive tor instance for its lifetime. Finally, collect the results
and post-process them (if applicable) before saving the data to the
results data store. If the experiment fails it should be possible to track
down the reason for failure from looking at the experiment results.
\item[request scheduler] Start new requests following a previously
configured schedule.
\item[request runner] Handle a single request from creation over various
possible sub states to timeout, failure, or completion.
\item[HTTP/SOCKS client] Make HTTP GET requests using our own SOCKS client
and connecting to a local tor client to gather as many timestamps of
substeps as possible.
\item[Selenium/Firefox wrapper] Make web request using Selenium/Firefox,
possibly using Xvfb and using a previously configured Firefox version or
binary, and collect as many timestamps of substeps as possible.
\item[SOCKS or HTTP proxy] Capture even more timestamps by proxying
requests made via Selenium/Firefox through our own SOCKS or HTTP proxy
before handing them to the local tor client.
\item[HTTP server] Run on port 80, serve static files and the canonical
median weg page, accept POST requests, provide measurement results via
RESTful API, present measurement results on web page.
\item[Alexa top-X web pages updater] Periodically retrieve list of top-X
web pages.
\item[results data store] Store request details, retrieve results,
periodically delete old results if configured, possibly using a database.
\item[results accumulator] Periodically collect results from other Torperf
instances, warn if they're out of date.
\item[analysis scripts] Command-line tools to process measurement results
and produce graphs and other aggregate statistics.
Could also become part of stem instead, together with a tutorial.
\end{description}

\section*{Acknowledgments}

Lunar, Zack Weinberg, and Rob van der Hoeven provided valuable feedback
for this report.

\bibliography{torperf2}

\appendix
\section{Data formats}

In this section we give examples of the suggested data formats to be
produced by Torperf.

\subsection{File download}

A file download document contains all data and meta data from making a
static file download over Tor.
It will only be used by the experiment specified in
\ref{sec:static-file-downloads}.
In the following, we give a file download example document and define its
fields:

\begin{longtable}{p{8.5cm}p{7cm}}
\verb+{+ \\
\verb+  "type": "file_download",+ &
Document type string; always \verb+"file_download"+ for this document
type; required.\\
\verb+  "guid": "ec2_9001_1365473921.75",+ &
Globally unique identifier for a file download document, consisting of
service identifier, tor SOCKS port, and request start time; required.\\
\verb+  "source": "ec2",+ &
Configured service identifier; required.\\
\verb+  "socks": 9001,+ &
Configured tor SOCKS port; required.\\
\verb+  "filesize": 51200,+ &
Configured file size in bytes; required.\\
\verb+  "tor_version": "0.2.2.35",+ &
Tor software version; optional.\\
\verb+  "start": 1365473921.75,+ &
Time when the connection process starts; required.\\
\verb+  "socket": 1365473921.75,+ &
Time when the socket was created; required.\\
\verb+  "connect": 1365473921.75,+ &
Time when the socket was connected; required.\\
\verb+  "negotiate": 1365473921.75,+ &
Time when SOCKS 5 authentication methods have been negotiated; required.\\
\verb+  "request": 1365473921.75,+ &
Time when the SOCKS request was sent; required.\\
\verb+  "response": 1365473922.07,+ &
Time when the SOCKS response was received; required.\\
\verb+  "datarequest": 1365473922.07,+ &
Time when the HTTP request was written; required.\\
\verb+  "dataresponse": 1365473922.40,+ &
Time when the first response was received; required.\\
\verb+  "datacomplete": 1365473922.76,+ &
Time when the payload was complete; required.\\
\verb+  "writebytes": 82,+ &
Total number of bytes written; required.\\
\verb+  "readbytes": 51323,+ &
Total number of bytes read; required.\\
\verb+  "didtimeout": False,+ &
True if the request timed out, False otherwise; optional.\\
\verb+  "dataperc": {+ &
Time when $x\%$ of expected bytes were read for
$x$ = \{ 10, 20, 30, 40, 50, 60, 70, 80, 90 \}; optional.\\
\verb+    "10": 1365473922.56,+\\
\verb+    "20": 1365473922.64,+\\
\verb+    "30": 1365473922.64,+\\
\verb+    "40": 1365473922.68,+\\
\verb+    "50": 1365473922.68,+\\
\verb+    "60": 1365473922.72,+\\
\verb+    "70": 1365473922.72,+\\
\verb+    "80": 1365473922.75,+\\
\verb+    "90": 1365473922.75 },+\\
\verb+  "stream_guid":+ &
Global unique identifier of the stream used for this measurement;
optional.\\
\verb+    "ec2_9001_1365473922.76_884"+\\
\verb+}+ & \\
\end{longtable}

\subsection{Stream}

A stream document contains all data and meta data from opening a stream,
attaching it to a circuit, maybe detaching and re-attaching it, to closing
it.
Any experiment can generate stream documents for the streams created by
its tor process.
In the following, we give a stream example document and define its fields:

\begin{longtable}{p{8.5cm}p{7cm}}
\verb+{+ \\
\verb+  "type": "stream",+ &
Document type string; always \verb+"stream"+ for this document type;
required.\\
\verb+  "guid": "ec2_9001_1365473922.76_884",+ &
Globally unique identifier for a stream document, consisting of service
identifier, tor SOCKS port, and stream creation time, and
locally unique stream identifier; required.\\
\verb+  "source": "ec2",+ &
Configured service identifier; required.\\
\verb+  "socks": 9001,+ &
Configured tor SOCKS port; required.\\
\verb+  "create": 1365473922.76,+ &
Time when the stream was created; required.\\
\verb+  "stream_id": 884,+ &
Locally unique stream identifier; required.\\
\verb+  "circuits": [+ &
Sequence of circuits that the stream was attached
to, each identified by its globally unique identifier and optionally
containing reason and remote reason for detaching; optional.\\
\verb+    { "circuit_guid":+\\
\verb+        "ec2_9001_1365473682.13_501",+\\
\verb+      "reason": "END",+\\
\verb+      "remote_reason": "DONE" } ]+\\
\verb+}+\\
\end{longtable}

\subsection{Circuit}

A circuit document contains all data and meta data about launching and
creating a circuit.
Any experiment can generate circuit documents for the circuits built by
its tor process.
In the following, we give a circuit example document and define its
fields:

\begin{longtable}{p{8.5cm}p{7cm}}
\verb+{+ \\
\verb+  "type": "circuit",+ &
Document type string; always \verb+"circuit"+ for this document type;
required.\\
\verb+  "guid": "ec2_9001_1365473682.13_501",+ &
Globally unique identifier for a circuit document, consisting of service
identifier, tor SOCKS port, circuit launch time, and
locally unique circuit identifier; required.\\
\verb+  "source": "ec2",+ &
Configured service identifier; required.\\
\verb+  "socks": 9001,+ &
Configured tor SOCKS port; required.\\
\verb+  "launch": 1365473682.13,+ &
Time when the circuit was launched; required.\\
\verb+  "circ_id": 501,+ &
Locally unique circuit identifier; required.\\
\verb+  "path": [+ &
Sequence of relays chosen for the circuit including
build times since launching the circuit; optional.\\
\verb+    { "fingerprint":+\\
\verb+        "$62680CF0743460E5F836F949E37A6DEC22622F9E",+\\
\verb+      "buildtime": 0.53 },+\\
\verb+    { "fingerprint":+\\
\verb+        "$11064D066F892DC38AAEFDA5EDAE1A227D07D182",+\\
\verb+      "buildtime": 1.10 },+\\
\verb+    { "fingerprint":+\\
\verb+        "$35F51DCE73B988CBAE06B06312B4D1271979FE3B",+\\
\verb+      "buildtime": 1.32 } ],+\\
\verb+  "buildtimeout": 4.83,+ &
Circuit build timeout used when building this
circuit; optional.\\
\verb+  "quantile": 0.800000+ &
Circuit build time quantile used to determine
the circuit build timeout; optional.\\
\verb+}+\\
\end{longtable}

\end{document}

