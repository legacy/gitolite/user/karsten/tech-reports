#!/usr/bin/python

from __future__ import print_function
import stem
from stem.control import EventType, Controller
import time
import sys

# handler for circuit events

def process_circ_event(event):
    if event.status in [stem.CircStatus.FAILED, stem.CircStatus.BUILT]:
        print("CIRCUIT", event.id, map(lambda x: x[1], event.path), event.purpose, event.status, event.reason, event.remote_reason, event.created, sep='\t')
        sys.stdout.flush()
        
SOCKS_PORT = 9050
CONTROL_PORT = 9051
tor_process = None

def local_tor_launch():
    import stem.process
    #Start up a Tor instance with the right SOCKS and control ports,
    # no guard nodes, (we want a sample of overall failure rate, not one guard's)
    # and assume a binary in the current dir (so we can run different binaries and test TAP vs ntor)
    tor_process = stem.process.launch_tor_with_config(
	tor_cmd = './tor',
        config = {
            'UseEntryGuards' : '0',
            'DataDirectory' : './.tor/',
            'SocksPort' : str(SOCKS_PORT),
            'ControlPort' : str(CONTROL_PORT),
            'SafeLogging' : '0',
            'CookieAuthentication': '1' },
        take_ownership = True)

t = time.gmtime()
print("TIME\t%d.%0.2d" % (t.tm_yday, t.tm_hour))
local_tor_launch()
num_circuits = 100
sleep_time = 15

with Controller.from_port(port=CONTROL_PORT) as controller:
    controller.authenticate()
    controller.add_event_listener(process_circ_event, EventType.CIRC)
    print("VERSION\t" + str(controller.get_version()))
    sys.stdout.flush()
    for circ_num in xrange(num_circuits):
        time.sleep(sleep_time)
        s1 = time.time()
        print("INIT", circ_num, time.strftime('%H:%M:%S', time.gmtime(s1)) + str(s1 % 1)[1:7], sep='\t')
        controller.new_circuit()

    time.sleep(sleep_time)
    controller.close()
