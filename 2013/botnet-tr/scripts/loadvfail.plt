set xrange [0:1]
set yrange [1:1000]
set logscale y
set ylabel "Onion-skin load"
set xlabel "Failure rate (p)"
set grid lw 4
set size 1.0, 0.5
set key top left
set terminal postscript portrait enhanced color dashed lw 1 "Helvetica" 18
set output "../graphs/loadvfail.eps"
plot (x**2 - 3*x + 3)/(1-x)**3 title 'Destroy' lw 4, 3/(1-x) title 'Reuse' with points
