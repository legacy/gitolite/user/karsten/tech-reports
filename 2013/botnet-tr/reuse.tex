\section{Client-side circuit-building adjustments} \label{sec:fail}

Although an adaptive botnet could always modify the Tor client code,
the regular user base still represents a large fraction of the load on
the network, so another set of potential solutions would be to focus
on modifying the behavior of ordinary clients to reduce the
circuit-building load.

\subsection{Can we reuse failed partial circuits?} 

Part of the problem caused by the heavy circuit-building load is that
when a circuit times out, the entire circuit is destroyed.  This means
that for every failed {\sc create}, at least three new
{\sc create} cells will be added to the network's load.  If we model
the entire Tor network as having probability $p$ of having a {\sc
  create} cell timeout, then the expected number of {\sc create} cells
needed to successfully build a circuit will be the $X_0$ satisfying
the linear system:
\begin{displaymath}
\begin{array}{llllll}
X_0 &=& pX_0  & + (1-p)X_1 & &+ 1 \\
X_1 &=& pX_0 &                   & + (1-p)X_2 & + 1\\
X_2 &=& pX_0 &                   &                  & + 1\ , 
\end{array}
\end{displaymath}
where $X_i$ is the expected number of cells to complete a partial
circuit with $i$ hops.  This gives us $X_0 = \frac{p^2 -3p +
  3}{(1-p)^3}\ .$

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{graphs/loadvfail}
\vspace{-10pt}
\caption{Expected onion-skin load per circuit created, for failure rate $p$}\label{fig:plot}
\end{center}
\vspace{-10pt}
\end{figure}
Conceptually, we can reduce this load by re-using a partially-built circuit,
e.g. when a timeout occurs, we truncate the circuit and attempt to
extend from the current endpoint.  In this case, the expected number
of {\sc create} cells needed to build a circuit will be simply
$X_0' = \frac{3}{1-p}$.  Figure~\ref{fig:plot} shows plots of both
functions.  We can see that for high enough failure rates, this change
causes a substantial reduction in load for the network.
Figure~\ref{fig:fail} shows typical failure rates for a stable (TAP)
and release candidate (ntor) roughly one month after the beginning of
the botnet event; we can see that at the observed failure rates
ranging from 10\%-25\%, reusing partial circuits would reduce the load
on the network by 10-30\%.

Of course, this model ignores the fact that failure probabilities are
neither static nor uniform across the entire Tor network, and the fact
that many nodes use ``create fast'' cells to exchange a first-hop key
without using Diffie-Hellman key exchange.  Reducing
the load introduced by failures will also reduce the rate of circuit
failures overall, but since CPU capacities vary widely across the Tor
network (and load balancing is by the essentially uncorrelated
bandwidth of nodes) the size of the actual effect due to this change
is difficult to predict.  Further evaluation will be needed.
Additionally, this change would also somewhat increase the power of
selective denial of service attacks~\cite{ccs07-doa}, although such
attacks typically only become noticeably effective in situations where
we would already consider Tor to be compromised.

\subsection{Can we build circuits less often?}

One reason for the relatively mild impact of the Mevade incident is
that, while hidden services initially build more circuits than
ordinary downloads, Tor is configured to aggressively avoid repeating
this process through the use of ``circuit dirtiness'' defaults.  When a
Tor client builds an ordinary circuit, is is marked as ``clean'' until
it is first used for traffic, at which point it becomes ``dirty.''
After a configurable amount of time (by default, 10 minutes) a
``dirty'' circuit cannot be used for new traffic and will be closed
after existing streams close.  In contrast, the ``dirty timer'' for a
hidden service circuit restarts every time it is used, so that as
long as a hidden service is visited once very 10 minutes, there is no
need to build a new circuit.  

Thus, another set of technical approaches to dealing with circuit
stress would be to investigate ways to reduce the number of circuits
that Tor clients build:
\begin{itemize}
\item Network consensus documents could include a ``recommended max
  dirtiness'' parameter that would adjust the lifetime of ordinary
  circuits, and a separate, potentially longer ``max dirtiness''
  parameter for hidden services.  The technical challenges here are: first, 
  balancing the tradeoffs between decreased anonymity for individual
  users (the longer a circuit is used, the more chance that some user
  activities will use the same circuit and be linked by an adversary)
  versus allowing more users on the network; and second, 
  reliably setting this parameter in an automated fashion.

\item Similarly, Tor clients currently build extra circuits for internal
  purposes like descriptor fetching and timeout testing, that could be
  disabled by an appropriately set consensus parameter.

\item Finally, while ``ordinary'' circuits are built preemptively for use when
  needed, some tasks such as hidden service connections, connecting to
  ``rare'' exit ports, and others currently build ``on-demand''
  circuits.  Finding ways to avoid constructing new circuits for
  these tasks, and analyzing the impact on anonymity and security,
  could reduce the amount of circuit building and also the perceived
  performance impact of a botnet.
\end{itemize}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "botnet-tr"
%%% End: 
