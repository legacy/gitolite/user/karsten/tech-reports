Users, especially those on smartphones, want privacy and the ability to bypass censorship for more
than just web browsing. Millions of people use chat and social media
applications, create and share media files like images and video, and more. In
fact, since Tor's performance penalties are most visible for delay-sensitive
applications like web browsing, we would like to explore secure messaging and
other asynchronous applications. We want to consider more use cases than just
web browsing.

\subsection{Existing apps that use Tor}
\label{apps-that-use-tor}

As a first step, we would like to understand whether existing prototypes
and tools currently using Tor work with Youtube and other popular services.
These tools include messaging and email apps, secure operating systems, tools
that send traffic from existing apps over the Tor network, monitoring tools,
and file sharing apps. 

\subsubsection{Web browsers}

In addition to Tor Browser, there are a number of web browsers that use the Tor network and protect users' privacy to varying degrees. These include OnionBrowser~\cite{onion-browser}, Brave~\cite{brave}, and Cliqz\cite{cliqz}.

\subsubsection{Using mobile apps with the Tor network}

The following apps send their internet traffic over the Tor network, whether by
using Tor's proxy interface, as Orbot does, or by providing a ``system-wide
VPN'' interface as iCepa does.

\paragraph{Orbot}

Orbot~\cite{orbot} is an open-source app that allows other apps to send their
internet traffic through it and then through the Tor network, thereby hiding
the user's IP address. It includes an expert mode that allows users to make
all network connections through Tor. However, it requires user caution
as it does not provide any additional anti-tracking measures -- it is trivial
for apps or websites to track users in most web browsers and many other apps
even if the IP address is hidden.

\paragraph{iCepa}

iCepa~\cite{icepa} is a new experimental app for connecting iOS apps to the Tor
network. iCepa is in alpha.

\subsubsection{Communications}

From peer to peer messaging apps, to email clients, there are a number
of different applications that people can use to anonymize their
communications:

\paragraph{Briar}

Briar~\cite{briar} is an open source peer-to-peer messaging app for Android.
Briar sends its messages through the Tor network, preventing third parties from
observing which parties are communicating with each other or revealing their IP
addresses.

\paragraph{Ricochet}

Ricochet~\cite{ricochet} is a desktop app that offers peer-to-peer messaging,
without exposing information about who is communicating. All messages are sent
wholly inside the Tor network. Ricochet uses Tor onion services to connect
users to one another, eliminating the central server that most messaging apps
use.

\paragraph{ChatSecure}
ChatSecure~\cite{chatsecure} is an open source iOS app offering optional Tor
support that hides the device's IP address and allows messages to bypass
restrictive firewalls. 

\paragraph{TorBirdy}

TorBirdy~\cite{tor-birdy} is an extension for Thunderbird, a desktop email
client, that is developed and maintained by members of the Tor Project.
TorBirdy sends email from Thunderbird over the Tor network. This means that the
email servers a message passes through do not learn the user's IP address. It
also hides email metadata to protect users' privacy. TorBirdy is in beta.

\subsubsection{Secure operating systems}

Currently there are a number of Linux-based operating systems which aim to
provide anonymity and privacy to their users. These OSes send their internet traffic through the Tor network. These include Tails (The Amnesic Incognito Live System)~\cite{tails}, Whonix~\cite{whonix}, and Qubes~\cite{qubes}. 

\subsubsection{Monitoring tools}

\paragraph{OONI Probe}

OONI Probe~\cite{ooni-probe} is an app that runs on Mac OS X, Linux, Android,
and iOS that allows users to run tests to determine if their internet
connection is being censored or otherwise experiencing interference.
OONI is a Tor project that monitors and reports on
censorship around the world. See Section~\ref{ooni} for more information.

\paragraph{Haven}
Haven~\cite{haven} is an Android app that uses device sensors to monitor a
physical space. It detects motion, sound, vibration and light, and watches for
unexpected guests and intruders. It offers a Tor onion service feature that
allows the operator to connect to the device remotely and view its event
logs and
captured media. Haven is currently in beta.

\subsubsection{File sharing}

There are a few applications currently available which allow anonymous file
sharing over the Tor network:

\paragraph{OnionShare}

OnionShare~\cite{onionshare} is a desktop app that allows users to send files to
one another over the Tor network. This prevents observers from seeing who is
sending and receiving the files.

\paragraph{GlobaLeaks}

GlobaLeaks~\cite{globaleaks} is software that enables people and organizations
to set up their own secure, anonymous whistleblower platforms. When a user
creates a new deployment, GlobaLeaks sets up a Tor onion service. People
submitting documents can use Tor Browser to maintain anonymity when connecting
to the platform.

There are a wide variety of GlobaLeaks deployments around the world for
purposes such as investigative journalism, anti-corruption activism,
transparency activism, and anti-crime activism.

\paragraph{SecureDrop}

SecureDrop~\cite{securedrop} is server software that allows whistleblowers to
send files anonymously and securely to journalists. It functions as a Tor onion
service and requires whistleblowers to use Tor Browser to submit materials.
Over the past few years, it has become very common for major news organizations
to deploy SecureDrop. The New York Times, The Associated Press, The Guardian,
and ProPublica are just a few of the organizations that use SecureDrop. 

\paragraph{OpenArchive}

OpenArchive is a new app for Android for uploading and sharing media, including
video. It sends its traffic over the Tor network to the Internet Archive and
other archives. OpenArchive looks promising, but we need to examine whether
Open Archive solves the problems that actual users have and assess its
usability at accomplishing these goals. 

\subsection{Applications for users under censorship}

We conducted a survey of the landscape of apps popular among users behind
censorship, and thought about how we can serve those users, considering both
safety and usability. Understanding what apps are popular with these users can
help us understand what would be useful to build, and what apps we should try
to make work with Tor. Internet censorship in China is particularly extensive, so if we consider solutions for users there, the lessons we learn may also apply in other contexts.

We began with the assumption that most users in most censored places would want
to be using popular Android apps, since Android is the world's most popular
mobile operating system and users in most places will have the same usage
habits as one another. Popular apps include messaging apps,
social media apps, app stores, and video streaming and sharing
apps~\cite{popular-android-apps-wiki}.

Many of these very popular apps such as WhatsApp, Telegram, Signal, Facebook,
Twitter, Netflix, Twitch, and Periscope are blocked in
China~\cite{china-blocked-apps}. In China, TenCent~\cite{tencent-wiki}
provides its own apps for messaging, social media, photo sharing, music
streaming, ebooks, e-sport streaming, and more~\cite{tencent-social}. It is
widely understood that Chinese government surveillance of these apps is
pervasive~\cite{tencent-surveillance}.

All of these types of services could benefit from a Tor-based offering to
protect privacy and fight censorship. There are quite a few mobile VPNs that
claim to do this, but many don't do it well or safely. Virtual private networks
(VPNs) are very popular in China for circumventing censorship, but Chinese
authorities have taken steps to ban their use~\cite{china-vpn-ban}, with varying degrees of success.

An obvious challenge is how to make apps and their updates available in places
where standard app stores such as Google Play and F-Droid are themselves
censored. 

Another important consideration is that people in China might not switch to
Tor to connect to a website or app because that website or app, as well as
Tor's documentation, lacks sufficient localization in Mandarin or Cantonese.
How do we address this problem with localization in mind? Do we simply assume
that people will generally switch over and add Chinese content back to the
larger web? Do we accept that we may only be serving English-speaking users in
China to some degree? Realistically we only have control over our own products,
and making translation apps usable through Tor, but it's an important thing to
bear in mind when trying to answer the question ``can we make this popular app
work with Tor for users in China?''

% We might use some of this later.
%
%Highly popular apps on Android devices (some TenCent alternatives are listed next to apps blocked in China):
%\begin{itemize}
%  \item Messaging
%  \begin{itemize}
%  \item WhatsApp (blocked in China) (TenCent: QQ/WeChat)
%  \item Facebook Messenger (blocked in China) (TenCent: QQ/WeChat)
%  \item Discord
%  \item Telegram( blocked in China) (TenCent: QQ/WeChat)
%  \item LINE (blocked in China) (TenCent: QQ/WeChat)
%  \item KaKao Talk (blocked in China) (TenCent: QQ/WeChat)
%  \item Signal (blocked in China) (TenCent: QQ/WeChat)
%  \end{itemize}
%  \item Video Chat
%  \begin{itemize}
%  \item Hangouts
%  \item Skype
%  \end{itemize}
%  \item Social Media
%  \begin{itemize}
%  \item Instagram
%  \item Facebook (blocked in China) (TenCent: QZone - also used to upload videos)
%  \item SnapChat
%  \item Twitter (blocked in China) (TenCent: Weibo, QZone - also used to upload videos)
%  \item Pinterest
%  \item Google+
%  \item Tinder
%  \end{itemize}
%  \item Content Browsing
%  \begin{itemize}
%  \item Google Play Books (blocked in China) (TenCent: China Reading Limited (Tencent Literature))
%  \item Google News
%  \item Reddit
%  \item Blogspot
%  \item Reddit
%  \item Flickr
%  \end{itemize}
%  \item Utilities
%  \begin{itemize}
%  \item Google Translate
%  \item Google Street View
%  \item Google Maps
%  \end{itemize}
%  \item Search Engines
%  \begin{itemize}
%  \item Search Engines
%  \item Google
%  \item DuckDuckGo
%  \item Yahoo
%  \item Bing (accessible in China but some search results are censored)
%  \end{itemize}
%  \item Streaming websites and apps
%  \begin{itemize}
%  \item Youtube
%  \item Netflix (blocked in China) \todo{(TenCent: TenCent Pictures, maybe?)}
%  \item Vimeo
%  \item Dailymotion
%  \item Twitch (blocked in China) (TenCent: Penguin e-Sports)
%  \item Periscope (NOW Live) (blocked in China) (TenCent: QZone)
%  \end{itemize}
%  \item{Email}
%  \begin{itemize}
%  \item Gmail
%  \end{itemize}
% \item Cloud Storage \& Productivity
%  \begin{itemize}
%  \item Dropbox
%  \item Google Drive
%  \item Google Docs
%  \item Scribd
%  \item Slack
%  \end{itemize}
%  \item News \& Information websites
%  \begin{itemize}
%  \item New York Times
%  \item The Economist
%  \item Bloomberg
%  \item Wall Street Journal
%  \item Reuters
%  \item Wikipedia (English)
%  \item The Guardian
%  \item Le Monde
%  \item CNN
%  \item BBC
%  \item ABC
%  \end{itemize}
%\end{itemize}
%
%
%\subsubsection{TenCent apps and services}
%
%\paragraph{WeChat}
%
%"Weixin/WeChat reached more than 938 million monthly active user accounts as of
%the first quarter of 2017."~\cite{tencent-social}
%
%\paragraph{QQ}
%
%QQ is an instant messaging app potentially aimed at younger users, complete
%with comics, games, and a payment service built in.
%
%"As of the first quarter of 2017, QQ had 861 million monthly active user
%accounts and more than 266 million people were using QQ
%simultaneously."~\cite{tencent-social}
%
%\paragraph{QZone}
%
%Qzone is a social media platform. It allows image and video upload, live
%streaming, games, blog posts, etc. Might be an important focus later on, given
%our one-sentence goal for this project.
%
%"Qzone Album records peak daily uploads of 650 million photos and total uploads
%of 2 trillion photos.As of the first quarter of 2017, Qzone had 632 million
%monthly active user accounts."~\cite{tencent-social}.
%
%\paragraph{China Reading Limited (Tencent Literature)}
%
%"China Reading Limited brings the full integration of online and traditional
%literary works to one platform."\cite{tencent-social}
%
%\paragraph{QQ Music}
%
%QQ Music is a music streaming service.\cite{tencent-social}
%
%\paragraph{Penguin e-sports}
%
%Penguin e-sports is a mobile live-streaming service for e-sports, similar to
%Twitch.\cite{tencent-social}
%
%\paragraph{NOW Live}
%
%NOW Live is a video live-streaming platform.\cite{tencent-social}
%
%\todo{expand research on the above platforms}
