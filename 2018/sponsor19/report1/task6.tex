\subsection{Tor network health}
\label{sec:network-health}

The health of the Tor network is crucial to all of the services Tor provides,
including the ability of users to bypass censorship. This section details
the health of the Tor network in terms of capacity, diversity, and performance.
Section~\ref{health-state} describes the current state of the network,
including some statistics and basic information on network health.

Section~\ref{health-bad} describes our ongoing fight against
malicious relays and includes our detection methods and our process to remove a
relay from the network.

\subsubsection{The state of the network}
\label{health-state}

\paragraph{Numbers}

The Tor network is currently composed of about 6400 relays run by
volunteers around the world. As Figure~\ref{png-network} shows, the
number of relays has been quite steady during 2018.

\begin{figure}
\begin{center}
  \includegraphics[width=0.9\textwidth]{task6/network.png}
\end{center}
\caption{Number of running relays and bridges in the public Tor network. For
relays, only those that have been verified as reachable by the directory
authorities are included. For bridges, only those that announce themselves to
the bridge authority are included (that is, private bridges are not included).}
\label{png-network}
\end{figure}

This graph includes bridges, which are relays that are not listed in the public
directory. In early July we transitioned to a new bridge authority, which is a
special relay that aggregates bridge information. Currently there are slightly
under 1000 bridges in the network.

The capacity of the Tor network is hovering around 275 GBit/s and around
125 GBit/s are actually in use. Figure~\ref{png-bw} shows an
increase in available bandwidth since March 2018.

\begin{figure}
\begin{center}
  \includegraphics[width=0.9\textwidth]{task6/bw.png}
\end{center}
\caption{The sum of advertised and consumed bandwidth across all relays
in the network.
The advertised bandwidth is the peak volume of traffic, both incoming
and outgoing, that a relay has observed itself able to do based on
recent data transfers. The consumed bandwidth is the volume
of traffic, both incoming and outgoing, that the relay has reported it has
transferred.}
\label{png-bw}
\end{figure}

As shown in Figure~\ref{png-user}, by our measurements the network is
currently seeing an estimated 2 million users each day. Since March,
this number has been quite steady. It is important to understand that this
count makes the conservative assumption that each of these users is online
for the entire day. If every user is instead online only a few hours each
day, our estimate could be low by a factor of ten.
Early reports from new
research~\cite{tor-usage-priv-measurement-imc18} show a much higher estimate and
we plan to take a close look at their methodology.

\begin{figure}
\begin{center}
  \includegraphics[width=0.9\textwidth]{task6/user.png}
\end{center}
\caption{Estimated number of daily directly-connecting clients; this
excludes clients connecting via bridges. These estimates come from
counting the number of directory requests reported by relays.}
\label{png-user}
\end{figure}

Notice the surge in users at the beginning of the year. This was due to a
denial of service attack that we discuss in Section~\ref{dos}. We can
ignore this for now.

Finally, Figure~\ref{png-user-bridge} shows the number of users using a bridge
to enter the network. The current estimate shows around 55,000 daily users. Again,
disregard the surge of users during the middle of the year; this was also the
result of a denial of service attack.

\begin{figure}
\begin{center}
  \includegraphics[width=0.9\textwidth]{task6/user-bridge.png}
\end{center}
\caption{Estimated number of daily clients connecting via bridges. These
numbers come from the number of directory requests reported by bridges.}
\label{png-user-bridge}
\end{figure}


\paragraph{Diversity}

Diversity is an important factor for the safety and anonymity provided by the
Tor network. We can measure diversity in a variety of ways, but for this
discussion, we focus on diversity of the operating systems (OSes) that relays
run on and the diversity of the physical locations of relays. 

The following simple example shows the importance of operating system
diversity: a software vulnerability that affects all Windows computers won't
threaten the security of relays that run on Linux. So in terms of security,
having relays that run on a wide variety of OSes improves the resilience and
safety of the Tor network.

Unfortunately, the vast majority of relays (over 90\%) are currently running on
the Linux OS. The rest are on variants of the BSD OS; very few are on
Windows. This is far from ideal for the network, and there is work to do to
make sure Tor is easy to install and configure on many platforms. One of our
current projects includes outreach and improved documentation to make it easier
to set up relays on BSD OSes.

Geographic diversity is important because it improves the anonymity the network
can provide, as well as its ability to help bypass censorship. If someone
watching network traffic, such as a government actor, can observe both the
connection to the guard relay and traffic exiting the Tor network, they may be
able to compromise the anonymity of the user. If the guard relay and the exit
relay are in different jurisdictions, there is much less chance of this.

Also, it is easier to censor a connection if, for example, an ISP can observe
a user connecting to the Tor network as well as the exit relay and determine
the user's web destination. If the exit relay is in a different jurisdiction
than the user, there is less chance that this type of censorship can succeed.
In general, spreading relays across different jurisdictions lessens the chance
that a single observer can see the entire path through the Tor network.

%Relays concentrated in only a few countries would be very bad for the
%anonymity of the network because it becomes much easier to observe from an
%attacker perspective than if the relays were evenly spread out around the
%world in many more countries that have different geo-political realities.
%
%This is true for entry, middle and exit relays of the network and it is
%critical for the safety of Tor users. 
%

Figure~\ref{png-exit-prob} shows the probability that a Tor
connection will exit the network in each continent. As you can see, there is a
high concentration of exit relays on the European continent.

\begin{figure}
\begin{center}
  \includegraphics[width=0.9\textwidth]{task6/exit-prob.png}
\end{center}
\caption{Exit probability by continent based on resolving relay addresses
using an IP geolocation database.}
\label{png-exit-prob}
\end{figure}

We use IP geolocation databases to determine where the exit relays are located,
and these are often not completely accurate, but we see that users will almost
always exit the network either in North America or Europe. This is far from
ideal and something we are working to improve. We've recently hired a relay
advocate who helps support current relay operators and does outreach to attract
new ones. In addition, as part of our global south initiative, our community
liaison has been helping volunteers set up relays in those regions. 

\subsubsection{Malicious relays}\label{health-bad}

This section describes the types of malicious relays we see in the network and
how we deal with them.

\paragraph{Rejecting relays}

Anyone on the Internet can run a relay which means that we need to be aware
that malicious actors can join anonymously and disrupt the network---sometimes
in ways we haven't imagined.

The nine Tor directory authorities come to a consensus every hour about which
relays in the network can be used and publish this list. If a majority of
directory authorities agree that a relay is behaving maliciously, it is removed
from the list.

The Tor community created a \emph{bad relay} team that is in charge of
detecting malicious relays and proposing reject rules to the directory
authorities. The directory authorities, in turn, decide whether to apply the rules so they really have the final word.

That team has a series of tools to probe the network in order to
detect malicious behavior by relays. Regularly, all of the 6400+ relays
are probed and tested for various things. Here are some of the current tests:

\begin{itemize}
  \item Stripping SSL certificate
  \item Re-writing/Redirecting plaintext Web page
  \item TLS Downgrade
  \item Bitcoin address rewrite
  \item Exit re-injecting traffic into the network
  \item Malicious onion service directory
\end{itemize}

There is, of course, much more that we could be doing, and we are always
looking for volunteers to help with this. In an ideal world, the Tor Project
would have funding to hire a \emph{network health engineer} to monitor
the network more closely and
improve our detection methods.

\paragraph{Malicious onion service directories}

A relay
that has been online for 96 hours or more can become an \emph{onion
service directory}.
Because of a design flaw in version 2 of the onion service protocol,
such a relay can learn addresses of onion services in the network. A
long-lived relay can learn many onion addresses over time.

This could enable the relay to launch a series of attacks in order to
de-anonymize onion service users. We put great effort into
designing version 3 of the protocol, which fixes this problem. But we still,
and will for years to come, have version 2 services running that are affected
by this issue.

For this reason, we have a scanner that detects onion service directories that
collect .onion addresses and visit them. Every week we reject 5 to 10 relays
from the network that are doing this.

\subsubsection{Current outlook and future directions}

The Tor network is healthy in terms of capacity.
% It is not currently using all
%of the bandwidth available to it and so has room to serve more users.
% ^ no, this isn't right: filling up all the capacity would be essentially
% impossible since any given circuit will have a bottleneck relay in it.
We have a
healthy number of relays that are spread out over many countries around the
world, which is crucial for protecting anonymity and evading censorship.

The number of Tor users is also important to the health of the system. One
can't be anonymous alone---good anonymity requires a crowd. As the number of
Tor users continues to grow, the anonymity provided by the network becomes
stronger. By working towards making Tor more accessible, more user friendly,
and more widely deployed on different devices (for example mobile), safety is
improved.

The Tor Project and its community have built tools and processes to monitor,
detect, and reject malicious relays, making the network safer. Onion service
version 3 is a great example of years of cooperation between the development
team, researchers, and our community to improve safety.

There is, of course, a lot more that we can do. Getting more exit relays
running in more places around the world is an important goal, along with adding
more OS diversity to the network. Improving our network monitoring is an
ongoing goal. The health of the network really depends on collaboration among
everyone in our community. 

\subsection{Denial of service}
\label{dos}

This section describes the denial of service attacks that the Tor network
faces. We'll use a recent example to demonstrate how important it is that our
community and monitoring work together.

\subsubsection{Notable attacks}
\label{dos-attacks}

In 2015, Nick Mathewson, Tor maintainer, released a technical report
\cite{dos-techreport} detailing different denial of service attacks on the Tor
network along with possible defenses that could be implemented. See section~\ref{perf-dos-attacks} for highlights from this report.

Over the years, the Tor Project and community have dealt with many attacks of
this kind, and they usually come as a surprise. This section highlights some of
the attacks we have faced and the lessons they have taught us.

\paragraph{Sefnit botnet}

On the 20th of August 2013, the number of users connected to the network
rose to abnormal levels~\cite{botnet-2013} (see Figure~\ref{dos-sefnit}).
Initial assessments of some relays showed a large number of clients trying to
connect to onion services.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{task6/sefnit.png}
  \end{center}
  \caption{Estimated number of users between August 2013 and January 2014.}
  \label{dos-sefnit}
\end{figure}

Although the network was still working fine, relays experienced higher than
normal use of their processors. This was traced to TAP~\cite{tor-spec-tap},
the protocol for establishing communication that the Tor client used.
NTor~\cite{tor-spec-ntor}, a new, more secure, and better performing protocol,
was about to be released in a new version of the client software.

Tor developers changed the relay software so that it prioritized the NTor
protocol. Old clients using TAP had less priority and were phased out.

Fortunately, by the end of 2013, the rise in users was identified by a
Microsoft researcher as part of the Sefnit botnet. It was removed by
Microsoft anti-virus at that point.

\paragraph{The sniper attack}

In February 2014, a paper was presented at the Network and Distributed
Systems Security symposium titled \textbf{The Sniper Attack: Anonymously
Deanonymizing and Disabling the Tor Network}~\cite{jansen2014sniper}.

It describes a way to remotely make a Tor relay ineffective using very few
resources. One of the most important aspects of this attack is that it can
be used very efficiently to deanonymize users, especially onion service users.

This is a concrete example of how a very cheap DoS could be used to mount other
types of attacks. The academic literature is years ahead of practice in terms
of attacks, so this is currently theoretical. Until we see it in practice, we
don't know what its actual effects might be.

The sniper attack showed us that taking down a relay could lead
to deanonymizing specific onion services.

\paragraph{DoS attack using onion services}

In mid December 2017, relay operators started to report to the tor-relays
mailing list that they were seeing spikes in traffic to their relays and
unusual amounts of memory and processing power being used.

This can sometimes happen in normal circumstances. For example if your relay
becomes the guard relay for the Facebook onion service, you'll see a surge in
resource usage in order to handle all of the users trying to reach it.

However, by the end of the month, the number of reports was unusually high,
which led us to investigate further. We also noticed that, at the same time,
there was a huge rise in the number of Tor users (see
Figure~\ref{dos-hs-2018}).

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{task6/hs-2018-dos.png}
  \end{center}
  \caption{Estimated number of daily users between August 2017 and March 2018.}
  \label{dos-hs-2018}
\end{figure}

After days of investigation, we realized that the entire network was being
bombarded by onion service requests. The sheer number was causing relays to
crash, to be shutdown by their operators to conserve resources, or at minimum,
to be rendered very slow or unusable.

We analyzed connection patterns to some of the affected relays and noticed that
there were 3 types of onion service connections that were coming to the relays
in large numbers:

\begin{enumerate}
  \item Client rendezvous requests
  \item Service rendezvous requests
  \item Tor2web rendezvous requests
\end{enumerate}

The attacker created hundreds of thousands of client onion service requests (1)
to specific onion services, which created a large number of rendezvous 
circuits (2), making the problem significantly worse.

We believe the Tor2web requests (3) were unrelated, although they started
around the same time.

Tor2Web is a gateway that allows users to access onion services from a regular
web browser. This speeds up connections for the user, but does not provide
anonymity. The so called ``DarkWeb scanners" attempt to scan all onion services.
They use Tor2web to increase their efficiency and thus scan the onion space
faster. This causes excessive memory and processing power to be used on the
relays that connect with the onion services.

In the face of this denial of service attack (DoS) we quickly rolled out 3
defenses which we implemented in all stable versions of Tor.

\begin{itemize}
  \item Limit concurrent connections
  \item Limit the rate at which circuits can be created
  \item Block Tor2web connections 
\end{itemize}

We observed that all of the malicious requests were coming from roughly 500-600
different IP addresses located within the networks of well known hosting
providers. This is probably because the cost of mounting a large DoS attack
from many IP addresses is much higher than only using relatively few addresses. 

Because thousands of requests were coming from the same IP address, limiting
the number of concurrent connections to guard relays from the same address
was an effective defense. In addition, after the limit on concurrent
connections was reached, we limited the rate at which guard relays could
contact other relays to form a circuit through the Tor network. We also blocked
connections from Tor2web.

%Furthermore, if a certain number of connections on the same IP was reached, the
%circuit creation rate limit defense would kick in and only allow you a
%specific rate and the rest would be denied. These were an attempts at soaking
%up large number of circuits/connections from clients using the same
%connections at the Guard node so it doesn't propagate into the network.

Once the majority of relays were running the updated software, the performance
of the network recovered quickly. After a few months, the attack stopped.
To this day, these defenses are still in place, protecting the Tor network
from such abuse.

\paragraph{Key takeaways}

These episodes took us by surprise and forced some Tor developers to drop
everything to investigate and work on defenses. But they produced some key
takeaways.

\begin{itemize}
  \item Large numbers of circuit requests create pressure not only on the
  network, but also on the Tor software. DoS attacks often highlight weaknesses
  in the application that can cause it to become unusable or to crash.
  Identifying these weaknesses prior to an attack is crucial to making Tor more
  resilient.

  \item The onion service design is clearly susceptible to DoS attacks. We have
  begun thinking about how we can solve these problems, but this is ongoing
  work.

  \item Listening to the Tor community is critical. Because Tor developers
  engage with the relay operators, we were able to notice that an attack was
  happening more quickly than if we had relied only on our own measurements.
  Working with our community has been instrumental in defending against most of
  the attacks we've experienced over the years.

  \item DoS attacks always have unpredictable effects, not only on the network
  but on the Tor software itself. These are very dangerous attacks that can
  quickly spiral out of control and we take them extremely seriously.
  \end{itemize}

\subsubsection{The future} \label{dos-future}

There are several important points to keep in mind when we consider how to
defend against future DoS attacks on the Tor network.

First, encouraging a wide and healthy research community is crucial.
Researchers often notice possible attacks before they are actually seen in
practice. Reacting to what researchers discover is important to keeping the
network strong.

Second, we need to interact constantly with our user and relay operator
communities. They have their fingers on the pulse of Tor network health and are
our most efficient ``monitoring device''.  When they report issues, it is because
they care about the Tor network. It is our job to listen to and work with them
to improve the safety and performance of the network.

Third, we need to improve our ability to understand what is going on inside the
Tor software. When we are faced with a potential DoS attack, extracting
relevant information quickly is critical. In order to do this we need proper
instrumentation in the Tor software. We don't currently have this, but in the
future, we need it.

Finally, we need the resources to defend against attacks. When these attacks
occur, developers need to drop everything to investigate and work on
fixes. The Tor Project needs to get to the point where it is free to work on
what is important for the Tor network and our users. We need to be able to
allocate resources towards defenses without jeopardizing our funding. Without
the ability for developers to take the time necessary to defend against these
attacks, the next one could be the one that takes down the network.
