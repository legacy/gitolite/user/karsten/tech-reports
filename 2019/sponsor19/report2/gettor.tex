\label{sec:gettor}

In cases where censors block access to the Tor Project website, users need
another method for downloading Tor Browser. GetTor, a service that provides a
way for a user to get Tor Browser from another site, was developed by an intern
participating in Google's Summer of Code. GetTor has gone unmaintained for the
past few years, but we now have plans and the necessary resources to improve
it and make it a standard Tor service.

GetTor can use multiple channels to receive requests and distribute links.
Email and Twitter have been the channels used in the past, but we are exploring
new channels, as well. GetTor also uses multiple providers to host the
Tor Browser software bundles that users download.

\subsection{Assess and improve distribution channels and software providers}

\subsubsection{Consider more channels and providers}

GetTor currently delivers links to the Tor Browser software through email and
over Twitter. We need to evaluate using other messaging platforms to deliver
links, such as XMPP, Telegram, Tox, Bitmessage, and Facebook. 

We currently use Google Drive, Github, and Dropbox to host the Tor Browser
software bundles and will be adding Gitlab shortly. We should consider
additional providers to host the bundles.

\maybeurl{https://bugs.torproject.org/28231}

\subsubsection{Twitter}

There are a few issues related to the Twitter distribution method that need
addressing.

\begin{itemize}

	\item The @get\_tor twitter bot is not responding to requests because the
	Twitter API has changed. We need to update the bot to accommodate these
	changes.

	\maybeurl{https://bugs.torproject.org/27330}

	\item We need to get the @get\_tor twitter bot verified so that users making
	requests can be confident that they are making requests through an official
	Tor Project channel.

	\maybeurl{https://bugs.torproject.org/20116}

\end{itemize}

\subsection{Improve software bundle distribution}

\subsubsection{Automate updates to providers}

In order to ensure that we are always serving links to the latest Tor Browser
packages, we need to automate the delivery of the packages to each of the
providers. We currently use Google Drive, Github, and Dropbox to host the
software bundles and will be adding Gitlab shortly.

As part of this work, we need to include checks to see if a bundle is already
stored at a given provider, and whether it should be replaced with a more
recent version.

\maybeurl{https://bugs.torproject.org/14744} \maybelinebreak
\maybeurl{https://bugs.torproject.org/22664}

\subsubsection{Create more secure cloud accounts}

The Github repository is part of the official Tor Project Github account, but
the accounts at other providers are currently personal accounts. We need to
create official Tor Project accounts for Google Drive and Dropbox and tighten
the security of these by adding two-factor authentication to them. This will
make it harder for an adversary to hijack the accounts in order to serve
inauthentic versions of Tor Browser.

\maybeurl{https://bugs.torproject.org/10692}

\subsubsection{Consider providing links to the alpha bundle}

We don't currently make the alpha version of Tor Browser available through
GetTor, but we should consider doing so because some users in censored
countries might want it. Along with the most recent version, we currently offer
the immediately previous version to handle cases where users have older links.
Space limitation in our cloud provider accounts may mean this decision is a
trade-off between offering this previous version or the alpha version.

\maybeurl{https://bugs.torproject.org/20770}

\subsection{Improve software integrity checks}

Because GetTor helps users retrieve software from a variety of locations with
varying levels of security, we need to make sure there are easy ways for users
to verify the integrity of the software they get.

\subsubsection{Improve instructions for checking software bundle integrity}

We need to provide simple, straightforward instructions to help users verify
the integrity of the software bundles they download. We want to do this by
providing a minimal explanation in the body of the message that emphasizes the
importance of completing the integrity check, as well as a link to a guide that
walks the users through doing the check.

\maybeurl{https://bugs.torproject.org/17425}

\subsubsection{Deliver checksums for the software bundles}

Sometimes GetTor needs to deliver the Tor Browser software over insecure
channels, for example, if the software bundle is too large to be allowed over
gmail. In cases like this, we need to deliver checksums for the software
bundles to the user so that they can check the integrity of their bundle. We
could do this by making it possible for users to request checksums from GetTor
or by automatically sending the appropriate checksum file along with the link
to the bundle.

\maybeurl{https://bugs.torproject.org/3980}

\subsubsection{Check integrity of hosted software periodically}

We should create an automated script that periodically downloads our software
from the cloud providers we use to serve it and checks its integrity. This
would alert us in case files are altered or otherwise become corrupted.

\maybeurl{https://bugs.torproject.org/17214}

\subsection{Improve translation coverage}

We need to add the GetTor service to our translation process and get it
translated into our standard languages. It is especially important that we have
translations for languages that are used in censorship-prone locations, such as
Arabic.

In addition, we currently have a Portuguese translation submitted by a
volunteer to our Trac bug reporting system. We need to get that transferred to
Transifex, which is the tool we currently use for translations.

\maybeurl{https://bugs.torproject.org/28233} \maybelinebreak
\maybeurl{https://bugs.torproject.org/28284} \maybelinebreak
\maybeurl{https://bugs.torproject.org/19693}

\subsection{Improve logging and monitoring}

\subsubsection{Add monitoring}

As part of our larger effort to monitor all of our anti-censorship
infrastructure, we need to start monitoring the GetTor service.

\maybeurl{https://bugs.torproject.org/30152}

\subsubsection{Improve log handling}

We need to update GetTor's logging to make it more useful for debugging and to
collect usage statistics. More specifically, we need to implement log levels to
control what information is collected; we need to set up the logrotate utility
so that logs are stored in a format that can be easily exported and parsed by
other utilities; and we need to extract some simple statistics about usage of
GetTor.

\maybeurl{https://bugs.torproject.org/28339}

\subsection{Improve documentation}

\subsubsection{Specification document}

There is an old draft of a specification document that was created several
years ago. We need to update it to reflect the current state of GetTor, as well
as our near-term plans for the project.

\maybeurl{https://bugs.torproject.org/3781}

\subsubsection{Documentation included with the code \bluesmallcaps{completed}}

We updated some of the GetTor documentation: 

\begin{itemize}

	\item \textbf{README.md} Cleaned up grammar.

	\item \textbf{distribution\_methods.txt} Indicated that the XMMP bot needs
	to be fixed and added several potential distribution methods for future
	consideration.

	\item \textbf{providers.txt} Added notes on Dropbox and Google Drive. Added
	Github.

\end{itemize}

\maybeurl{https://bugs.torproject.org/28234}

\subsection{Improve the code base}

\subsubsection{Use the Twisted framework \bluesmallcaps{completed}}

We have refactored the GetTor code to create a Twisted daemon.  We have chosen
the Twisted framework because it handles all of the necessary networking
functionality and includes a good logging system. Using Twisted for GetTor
brings the service in line with the BridgeDB service, possibly simplifying
maintenance.

Each distribution channel (for example, email or Twitter) consists of one
or more services that store or retrieve requests from a database. Another
service is responsible for making sure the links GetTor serves point to
the latest version of Tor Browser.

\maybeurl{https://bugs.torproject.org/28152}

\subsubsection{Port to Python 3 \bluesmallcaps{completed}}

GetTor was originally written in Python 2, which goes out of support in 2020.
In preparation, we ported GetTor to Python 3.

\maybeurl{https://bugs.torproject.org/28091}

\subsubsection{Implement test functionality}

We need to implement unit tests and ensure adequate coverage of the code base.

\maybeurl{https://bugs.torproject.org/1593}
